﻿
-- ТЕСТ ДЛЯ ПРОВЕРКИ GIT
-- ТЕСТ 2  ДЛЯ ПРОВЕРКИ GIT
 
set nocount, xact_abort on;

declare 
        -- переменные для работы
		@create_user_id int = 111,
        @dt datetime = getdate(), 
        @bo_id int = 0, 
        @cnt_bo int, 
		-- параметры загрузки отчёта на стенд
        @fr_source varchar(max),                                                            -- XML-тело отчёта
        @nm_report varchar(255) = 'MDR',												    -- Наименование отчёта, которое появляется в меню
        @nm_alias varchar(255) = 'DbMsSql',                                                 -- псевдоним БД на сервисе ServiceFastReport
        @type varchar(255) = 'table',                                                       -- тип (table - выводим все записи, form - фильтруем по ключевому полю БО (по парасетру с таким же наименованием))
        @fr_descr varchar(255) = '(v23.08.10) MDR',                                         -- Описание (версия + БО)
        @action varchar(255) = 'csi_group_engineering.csi_tbl_screen_forms_template_mdr',   -- БО - action_code
		@action_code_report varchar(255) = 'MDR',                                           -- action_code_report отчёта (лучше например frx_eng_mdr)
        @fr_param varchar(800) = '[]',                                                      -- параметры в формате json
        @mask_file varchar(max) = null,                                                     -- mask_file имени отчёта (если пусто, - выгружаемый файл именуется action_code_report+guid)
		--Пример использования маски наименования отчёта
        --@fr_param varchar(800) = '[{"ParameterName":"transmittal_out_id","ParameterValue":""}]',
        --@mask_file varchar(max) = '{"table_name": "csi.csi_tbl_transmittal_out", "id": "transmittal_out_id", "param_name": "transmittal_out_id", "result": "concat(''Номер РМЗ '', rmz_number) as rslt"}';   
        @meta_description varchar(max) = null,                                              --
		@author_sys varchar(255) = null,                                                    --
		@csi_rec_code varchar(255) = null;                                                  --

set @fr_source = 
'<?xml version="1.0" encoding="utf-8"?>
<Report ScriptLanguage="CSharp" ReportInfo.Created="08/09/2023 13:08:43" ReportInfo.Modified="08/10/2023 18:50:27" ReportInfo.CreatorVersion="2023.2.0.0">
  <Dictionary>
    <MsSqlDataConnection Name="Connection" ConnectionString="rijcmlqYds/3sNQPlSJevqJ33Rom0eUz9YNOaXvNF1kPnn7vIbz+0B4yBfpZqt4t+aU7GrD+Btza6avi2XMmmbmJEQg39dtl35PrOD4BOQtXQcU2sIPPrADuDG+khhobvVrrKPwaovRZyxUzDtSI+xcPi6mwMdZOTlOLUQeA8GsyoZFzAzQnbe8C+fhjWlI0BM3via3xrztqv9sfc18pAVyEAkHTQ==" CommandTimeout="0">
      <TableDataSource Name="Table" DataType="System.Int32" Enabled="true" SelectCommand="select b.*, &#13;&#10;  s.participant_title_ru, ss.participant_title_ru participant_title_ru1,&#13;&#10;  t.line, t.mdr_tech_unit_code, t.mdr_sequence_num, t.mdr_type, t.title, t.title_title, t.title_title_en title_title_en1,&#13;&#10;  d.mark, ds.discipline_code, m.mark_code, doc.document_type_code, target.release_target, cast(b.progress as float) progress1&#13;&#10;from csi.csi_tbl_mdr b&#13;&#10;left join csi.csi_tbl_project_participant s on  s.project_participant_id = b.project_participant_sub_link&#13;&#10;left join csi.csi_tbl_project_participant ss on  ss.project_participant_id = b.project_participant_subsub_link&#13;&#10;left join csi.csi_tbl_title_object t on b.title_object_link = t.title_object_id&#13;&#10;left join csi.csi_tbl_discipline_design d on d.discipline_design_id = b.discipline_design_link&#13;&#10;left join csi.csi_tbl_discipline ds on ds.discipline_id = b.discipline_link&#13;&#10;left join csi.csi_tbl_mark_rd m on m.mark_rd_id = b.mark_rd_link&#13;&#10;left join csi.csi_tbl_document_type doc on doc.document_type_id = b.document_type_link&#13;&#10;left join csi.csi_tbl_release_target target on target.release_target_id = b.release_target_link&#13;&#10;where b.dt_delete is null">
        <Column Name="sft_id" DataType="System.Int32"/>
        <Column Name="dt_create" DataType="System.DateTime"/>
        <Column Name="dt_update" DataType="System.DateTime"/>
        <Column Name="dt_delete" DataType="System.DateTime"/>
        <Column Name="author_sys" DataType="System.String"/>
        <Column Name="guid" DataType="System.Guid"/>
        <Column Name="create_user_id" DataType="System.Int32"/>
        <Column Name="design_stage_code" DataType="System.String"/>
        <Column Name="doc_name" DataType="System.String"/>
        <Column Name="title_object" DataType="System.String"/>
        <Column Name="stage" DataType="System.String"/>
        <Column Name="launch_complex" DataType="System.String"/>
        <Column Name="PD_cipher" DataType="System.String"/>
        <Column Name="code_brand" DataType="System.String"/>
        <Column Name="discipline" DataType="System.String"/>
        <Column Name="developer" DataType="System.String"/>
        <Column Name="kit_number" DataType="System.String"/>
        <Column Name="factory_position_number" DataType="System.String"/>
        <Column Name="doc_type" DataType="System.String"/>
        <Column Name="doc_name_rus" DataType="System.String"/>
        <Column Name="doc_name_en" DataType="System.String"/>
        <Column Name="lang" DataType="System.String"/>
        <Column Name="doc_status_final" DataType="System.String"/>
        <Column Name="release_porpose" DataType="System.String"/>
        <Column Name="revision" DataType="System.String"/>
        <Column Name="doc_number_inter" DataType="System.String"/>
        <Column Name="doc_number_customer" DataType="System.String"/>
        <Column Name="doc_weight" DataType="System.Double"/>
        <Column Name="labor_cost_plan" DataType="System.Double"/>
        <Column Name="equimpent_number" DataType="System.String"/>
        <Column Name="doc_status" DataType="System.String"/>
        <Column Name="ksg_operation" DataType="System.String"/>
        <Column Name="plan_start" DataType="System.DateTime"/>
        <Column Name="plan_complete" DataType="System.DateTime"/>
        <Column Name="forecast_start" DataType="System.DateTime"/>
        <Column Name="forecast_complete" DataType="System.DateTime"/>
        <Column Name="actual_start" DataType="System.DateTime"/>
        <Column Name="actual_complete" DataType="System.DateTime"/>
        <Column Name="csi_rec_code" DataType="System.String"/>
        <Column Name="progress" DataType="System.String"/>
        <Column Name="title_object_link" DataType="System.Int32"/>
        <Column Name="ewp_link" DataType="System.Int32"/>
        <Column Name="bom_method_calculation" DataType="System.String"/>
        <Column Name="agreement_num" DataType="System.String"/>
        <Column Name="mdr_position_status" DataType="System.String"/>
        <Column Name="mdr_position_status_update_reason" DataType="System.String"/>
        <Column Name="note" DataType="System.String"/>
        <Column Name="mark_rd_begin_date_plan" DataType="System.DateTime"/>
        <Column Name="mark_rd_begin_date" DataType="System.DateTime"/>
        <Column Name="progress_5" DataType="System.String"/>
        <Column Name="progress_5_pause_reason" DataType="System.String"/>
        <Column Name="key_task_date_1_plan" DataType="System.DateTime"/>
        <Column Name="key_task_1_date" DataType="System.DateTime"/>
        <Column Name="progress_20" DataType="System.String"/>
        <Column Name="progress_20_pause_reason" DataType="System.String"/>
        <Column Name="key_task_date_2_plan" DataType="System.DateTime"/>
        <Column Name="key_task_date_2" DataType="System.DateTime"/>
        <Column Name="progress_35" DataType="System.String"/>
        <Column Name="progress_35_pause_reason" DataType="System.String"/>
        <Column Name="mark_check_normcontrol_issue_date_plan" DataType="System.DateTime"/>
        <Column Name="mark_check_normcontrol_issue_date" DataType="System.DateTime"/>
        <Column Name="progress_55" DataType="System.String"/>
        <Column Name="progress_55_pause_reason" DataType="System.String"/>
        <Column Name="mark_check_customer_1_letter_rev_issue_date_plan" DataType="System.DateTime"/>
        <Column Name="mark_check_customer_1_letter_rev_issue_date" DataType="System.DateTime"/>
        <Column Name="progress_60" DataType="System.String"/>
        <Column Name="progress_60_pause_reason" DataType="System.String"/>
        <Column Name="mark_check_normcontrol_rev_0_issue_date_plan" DataType="System.DateTime"/>
        <Column Name="mark_check_normcontrol_rev_0_issue_date" DataType="System.DateTime"/>
        <Column Name="progress_85" DataType="System.String"/>
        <Column Name="progress_85_pause_reason" DataType="System.String"/>
        <Column Name="mark_approve_customer_rev_0_date_plan" DataType="System.DateTime"/>
        <Column Name="mark_approve_customer_rev_0_date" DataType="System.DateTime"/>
        <Column Name="progress_90" DataType="System.String"/>
        <Column Name="progress_90_pause_reason" DataType="System.String"/>
        <Column Name="mark_approve_customer_rev_xx_date_plan" DataType="System.DateTime"/>
        <Column Name="mark_approve_customer_rev_xx_date" DataType="System.DateTime"/>
        <Column Name="progress_100" DataType="System.String"/>
        <Column Name="progress_100_pause_reason" DataType="System.String"/>
        <Column Name="list_format" DataType="System.String"/>
        <Column Name="list_count" DataType="System.String"/>
        <Column Name="review_code" DataType="System.String"/>
        <Column Name="document_class" DataType="System.String"/>
        <Column Name="gip_responsible" DataType="System.String"/>
        <Column Name="list_format_a4" DataType="System.String"/>
        <Column Name="construction_link" DataType="System.Int32"/>
        <Column Name="design_stage_code_link" DataType="System.Int32"/>
        <Column Name="cipher_pd_link" DataType="System.Int32"/>
        <Column Name="mark_rd_link" DataType="System.Int32"/>
        <Column Name="discipline_link" DataType="System.Int32"/>
        <Column Name="document_type_link" DataType="System.Int32"/>
        <Column Name="release_target_link" DataType="System.Int32"/>
        <Column Name="project_num" DataType="System.String"/>
        <Column Name="document_statuts_sequence" DataType="System.String"/>
        <Column Name="document_key" DataType="System.String"/>
        <Column Name="dl" DataType="System.String"/>
        <Column Name="document_num_serial" DataType="System.String"/>
        <Column Name="gate" DataType="System.String"/>
        <Column Name="lod_position" DataType="System.String"/>
        <Column Name="set_man_hour_plan" DataType="System.String"/>
        <Column Name="man_hour_plan" DataType="System.String"/>
        <Column Name="adress_customer_last_update_date_fact" DataType="System.DateTime"/>
        <Column Name="adress_customer_last_transmittal_num" DataType="System.String"/>
        <Column Name="transmittal_stamp" DataType="System.String"/>
        <Column Name="transmittal_date" DataType="System.String"/>
        <Column Name="responsible" DataType="System.String"/>
        <Column Name="project_participant_sub_link" DataType="System.Int32"/>
        <Column Name="project_participant_subsub_link" DataType="System.Int32"/>
        <Column Name="tech_unit_code" DataType="System.String"/>
        <Column Name="number_suqeunce" DataType="System.String"/>
        <Column Name="type" DataType="System.String"/>
        <Column Name="title_title_ru" DataType="System.String"/>
        <Column Name="title_title_en" DataType="System.String"/>
        <Column Name="discipline_design" DataType="System.String"/>
        <Column Name="result_type" DataType="System.String"/>
        <Column Name="discipline_weight" DataType="System.Double"/>
        <Column Name="transmittal_num" DataType="System.String"/>
        <Column Name="approve_code" DataType="System.String"/>
        <Column Name="date" DataType="System.DateTime"/>
        <Column Name="transmittal_out_number" DataType="System.String"/>
        <Column Name="crs_reissue" DataType="System.DateTime"/>
        <Column Name="approve_code_previous" DataType="System.String"/>
        <Column Name="discipline_design_link" DataType="System.Int32"/>
        <Column Name="start_id" DataType="System.String"/>
        <Column Name="start_plan_date" DataType="System.DateTime"/>
        <Column Name="start_forecast_date" DataType="System.DateTime"/>
        <Column Name="start_fact_date" DataType="System.DateTime"/>
        <Column Name="start_progress" DataType="System.Double"/>
        <Column Name="fiotd_id" DataType="System.String"/>
        <Column Name="fiotd_plan_date" DataType="System.DateTime"/>
        <Column Name="fiotd_forecast_date" DataType="System.DateTime"/>
        <Column Name="fiotd_fact_date" DataType="System.DateTime"/>
        <Column Name="fiotd_transmittal_num_customer" DataType="System.String"/>
        <Column Name="fiotd_progress" DataType="System.Double"/>
        <Column Name="orcofiod_plan_date" DataType="System.DateTime"/>
        <Column Name="orcofiod_forecast_date" DataType="System.DateTime"/>
        <Column Name="orcofiod_fact_date" DataType="System.DateTime"/>
        <Column Name="orcofiod_transmittal_num_customer" DataType="System.String"/>
        <Column Name="orcofiod_review_code_customer" DataType="System.String"/>
        <Column Name="siotd_plan_date" DataType="System.DateTime"/>
        <Column Name="siotd_forecast_date" DataType="System.DateTime"/>
        <Column Name="siotd_fact_date" DataType="System.DateTime"/>
        <Column Name="siotd_transmittal_num_customer" DataType="System.String"/>
        <Column Name="siotd_progress" DataType="System.Double"/>
        <Column Name="orcosiod_plan_date" DataType="System.DateTime"/>
        <Column Name="orcosiod_forecast_date" DataType="System.DateTime"/>
        <Column Name="orcosiod_fact_date" DataType="System.DateTime"/>
        <Column Name="orcosiod_transmittal_customer" DataType="System.String"/>
        <Column Name="orcosiod_review_code_customre" DataType="System.String"/>
        <Column Name="finiotd_plan_date" DataType="System.DateTime"/>
        <Column Name="finiotd_forecast_date" DataType="System.DateTime"/>
        <Column Name="finiotd_fact_date" DataType="System.DateTime"/>
        <Column Name="finiotd_transmittal_num_customer" DataType="System.String"/>
        <Column Name="finiotd_progress" DataType="System.Double"/>
        <Column Name="progress_date_plan" DataType="System.Double"/>
        <Column Name="progress_date_forecast" DataType="System.Double"/>
        <Column Name="progress_date_fact" DataType="System.Double"/>
        <Column Name="progress_date_deviation" DataType="System.Double"/>
        <Column Name="progress_weight_deviation" DataType="System.Double"/>
        <Column Name="progress_by_doc_date_plan" DataType="System.Double"/>
        <Column Name="progress_by_doc_date_forecast" DataType="System.Double"/>
        <Column Name="progress_by_doc_date_fact" DataType="System.Double"/>
        <Column Name="progress_by_doc_date_deviation" DataType="System.Double"/>
        <Column Name="finiotd_id" DataType="System.String"/>
        <Column Name="participant_title_ru" DataType="System.String"/>
        <Column Name="participant_title_ru1" DataType="System.String"/>
        <Column Name="line" DataType="System.String"/>
        <Column Name="mdr_tech_unit_code" DataType="System.String"/>
        <Column Name="mdr_sequence_num" DataType="System.String"/>
        <Column Name="mdr_type" DataType="System.String"/>
        <Column Name="title" DataType="System.String"/>
        <Column Name="title_title" DataType="System.String"/>
        <Column Name="title_title_en1" DataType="System.String"/>
        <Column Name="mark" DataType="System.String"/>
        <Column Name="discipline_code" DataType="System.String"/>
        <Column Name="mark_code" DataType="System.String"/>
        <Column Name="document_type_code" DataType="System.String"/>
        <Column Name="release_target" DataType="System.String"/>
        <Column Name="progress1" DataType="System.Double"/>
      </TableDataSource>
    </MsSqlDataConnection>
  </Dictionary>
  <ReportPage Name="Page1" PaperWidth="950" PaperHeight="600" Watermark.Font="Arial, 60pt" UnlimitedHeight="true" LastPageSource="15" FirstPageSource="15">
    <ReportTitleBand Name="ReportTitle1" Width="3515.4" Height="274.05">
      <TextObject Name="Text1" Width="3477.6" Height="18.9" Text="MDR / Главный реестр документации" HorzAlign="Center" VertAlign="Center" Font="Calibri, 10pt, style=Bold"/>
      <TableObject Name="Table1" Top="18.9" Width="3515.4" Height="255.15">
        <TableColumn Name="Column1" Width="37.8"/>
        <TableColumn Name="Column2" Width="37.8"/>
        <TableColumn Name="Column3" Width="75.6"/>
        <TableColumn Name="Column4" Width="37.8"/>
        <TableColumn Name="Column5" Width="37.8"/>
        <TableColumn Name="Column6" Width="37.8"/>
        <TableColumn Name="Column7" Width="37.8"/>
        <TableColumn Name="Column8" Width="37.8"/>
        <TableColumn Name="Column9" Width="75.6"/>
        <TableColumn Name="Column10" Width="113.4"/>
        <TableColumn Name="Column11" Width="113.4"/>
        <TableColumn Name="Column12" Width="75.6"/>
        <TableColumn Name="Column13" Width="37.8"/>
        <TableColumn Name="Column14" Width="37.8"/>
        <TableColumn Name="Column15" Width="37.8"/>
        <TableColumn Name="Column16" Width="37.8"/>
        <TableColumn Name="Column17" Width="56.7"/>
        <TableColumn Name="Column18" Width="37.8"/>
        <TableColumn Name="Column19" Width="37.8"/>
        <TableColumn Name="Column20" Width="37.8"/>
        <TableColumn Name="Column21" Width="75.6"/>
        <TableColumn Name="Column22" Width="75.6"/>
        <TableColumn Name="Column23" Width="37.8"/>
        <TableColumn Name="Column24" Width="37.8"/>
        <TableColumn Name="Column25" Width="151.2"/>
        <TableColumn Name="Column26" Width="151.2"/>
        <TableColumn Name="Column27" Width="151.2"/>
        <TableColumn Name="Column28" Width="37.8"/>
        <TableColumn Name="Column29" Width="56.7"/>
        <TableColumn Name="Column30" Width="37.8"/>
        <TableColumn Name="Column31" Width="75.6"/>
        <TableColumn Name="Column32" Width="132.3"/>
        <TableColumn Name="Column33" Width="37.8"/>
        <TableColumn Name="Column34" Width="75.6"/>
        <TableColumn Name="Column35" Width="132.3"/>
        <TableColumn Name="Column36" Width="75.6"/>
        <TableColumn Name="Column37" Width="37.8"/>
        <TableColumn Name="Column38" Width="151.2"/>
        <TableColumn Name="Column39" Width="75.6"/>
        <TableColumn Name="Column40" Width="37.8"/>
        <TableColumn Name="Column41" Width="37.8"/>
        <TableColumn Name="Column42" Width="75.6"/>
        <TableColumn Name="Column43" Width="37.8"/>
        <TableColumn Name="Column44" Width="151.2"/>
        <TableColumn Name="Column45" Width="37.8"/>
        <TableColumn Name="Column46" Width="75.6"/>
        <TableColumn Name="Column47" Width="151.2"/>
        <TableColumn Name="Column48" Width="113.4"/>
        <TableColumn Name="Column49" Width="151.2"/>
        <TableRow Name="Row1" Height="37.8">
          <TableCell Name="Cell1" Fill.Color="White" Font="Calibri, 8pt" RowSpan="3"/>
          <TableCell Name="Cell2" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Document key" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell3" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Deviation Flag" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell4" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Originator" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell5" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Project breakdown structure (Title) / &#13;&#10;Структура проекта (Титул)" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt" ColSpan="7"/>
          <TableCell Name="Cell16" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell17" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell18" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell19" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell20" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell21" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell22" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Engineering Discipline (WBS L4)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell23" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Discipline code (WBS L5)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell24" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Detailed design mark" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell25" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Type of document" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell26" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Document serial number" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell27" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Document Class" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell28" Border.Lines="All" Fill.Color="WhiteSmoke" Text="GATE" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell29" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Language" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell30" Border.Lines="All" Fill.Color="WhiteSmoke" Text="LOD Reference" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell31" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Type of Deliverable" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell32" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Document status sequences" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell33" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Planned manhours" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell34" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Payment Milestone No." HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell35" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Document Number" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell36" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Document Title (EN)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell37" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Document Title (RU)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell38" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Last issue / &#13;&#10;Последний выпуск" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt" ColSpan="5"/>
          <TableCell Name="Cell39" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell40" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell41" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell42" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell43" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Last Issue Owner review / &#13;&#10;Статус рассмотрения последнего выпуска Заказчиком" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt" ColSpan="5"/>
          <TableCell Name="Cell44" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell45" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell46" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell47" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell48" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Stamping&#10;(TRM)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell49" Border.Lines="All" Fill.Color="WhiteSmoke" Text="TRM Date" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell50" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Language" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell51" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Final Issue for Review&#13;&#10;(if applicable) " HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt" ColSpan="4"/>
          <TableCell Name="Cell52" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell53" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell54" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell55" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Финальный выпуск /&#13;&#10;Owner review" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt" ColSpan="3"/>
          <TableCell Name="Cell56" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell57" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell58" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Responsible" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt" RowSpan="2"/>
          <TableCell Name="Cell59" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Comments" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt" RowSpan="2"/>
        </TableRow>
        <TableRow Name="Row2" Height="94.5">
          <TableCell Name="Cell6" Fill.Color="White" Font="Calibri, 8pt"/>
          <TableCell Name="Cell7" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell8" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell9" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell10" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Process train number" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell60" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Construction area" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell61" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Index number" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell62" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Type" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell63" Border.Lines="All" Fill.Color="WhiteSmoke" Text="CWA number (Title)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell64" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Description (EN)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell65" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Description (RU)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell66" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell67" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell68" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell69" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell70" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell71" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell72" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell73" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell74" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell75" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell76" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell77" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell78" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell79" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell80" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell81" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell82" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Gate" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell83" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Document Status" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell84" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Issue" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell85" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Actual Date" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell86" Border.Lines="All" Fill.Color="WhiteSmoke" Text="TRM number" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell87" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Code" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell88" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Date" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell89" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Out TRM number" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell90" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Resubmission due to CRS" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell91" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Prior Review Cod" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell92" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell93" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell94" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell95" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Document Status" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell96" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Actual Date" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell97" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Issue" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell98" Border.Lines="All" Fill.Color="WhiteSmoke" Text="TRM&#13;&#10;number" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell99" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Code" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell100" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Date" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell101" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Out TRM number" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell102" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell103" Border.Lines="All" Fill.Color="WhiteSmoke" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
        </TableRow>
        <TableRow Name="Row3" Height="122.85">
          <TableCell Name="Cell11" Fill.Color="White" Font="Calibri, 8pt"/>
          <TableCell Name="Cell12" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Ключ документа" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell13" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Флаг отклонения " HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell14" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Субподрядчик" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell15" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Номер технологической линии" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell104" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Строительная зона" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell105" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Порядковый номер" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell106" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Тип" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell107" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Номер CWA (Титул)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell108" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Наименование (EN)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell109" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Наименование (RU)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell110" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Дисциплина по проектированию (WBS L4)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell111" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Код дисциплины (WBS L5)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell112" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Марка" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell113" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Тип документа" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell114" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Порядковый номер документа" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell115" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Класс документа" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell116" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Gate" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell117" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Язык" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell118" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Позиция LOD" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell119" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Тип результата" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell120" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Последовательность статусов документа" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell121" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Запланированные человеко-часы" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell122" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Платежная веха &#10;№" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell123" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Номер документа" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell124" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Наименование документа (EN)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell125" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Наименование документа (RU)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell126" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Gate" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell127" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Статус документа" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell128" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Выпуск" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell129" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Фактическая дата" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell130" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Номер трансмиттала" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell131" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Код рассмотрения Заказчиком" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell132" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Дата" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell133" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Номер&#13;&#10;трансмиттала" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell134" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Перевыпуск по CRS" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell135" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Предыдущий Код согласования" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell136" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Штамп&#10;(трансмиттал)" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell137" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Дата трансмиттала" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell138" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Язык" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell139" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Статус документа" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell140" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Фактическая дата" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell141" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Номер выпуска" HorzAlign="Center" VertAlign="Center" Angle="270" Font="Calibri, 8pt"/>
          <TableCell Name="Cell142" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Номер&#13;&#10;трансмиттала" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell143" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Код" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell144" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Дата" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell145" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Номер исх. трансмиттала" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell146" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Ответственный" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell147" Border.Lines="All" Fill.Color="WhiteSmoke" Text="Комментарии" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
        </TableRow>
      </TableObject>
    </ReportTitleBand>
    <PageHeaderBand Name="PageHeader1" Top="278.05" Width="3515.4" Height="11.34" CanGrow="true">
      <TableObject Name="Table3" Width="3515.4" Height="11.34">
        <TableColumn Name="Column99" Width="37.8"/>
        <TableColumn Name="Column100" Width="37.8"/>
        <TableColumn Name="Column101" Width="75.6"/>
        <TableColumn Name="Column102" Width="37.8"/>
        <TableColumn Name="Column103" Width="37.8"/>
        <TableColumn Name="Column104" Width="37.8"/>
        <TableColumn Name="Column105" Width="37.8"/>
        <TableColumn Name="Column106" Width="37.8"/>
        <TableColumn Name="Column107" Width="75.6"/>
        <TableColumn Name="Column108" Width="113.4"/>
        <TableColumn Name="Column109" Width="113.4"/>
        <TableColumn Name="Column110" Width="75.6"/>
        <TableColumn Name="Column111" Width="37.8"/>
        <TableColumn Name="Column112" Width="37.8"/>
        <TableColumn Name="Column113" Width="37.8"/>
        <TableColumn Name="Column114" Width="37.8"/>
        <TableColumn Name="Column115" Width="56.7"/>
        <TableColumn Name="Column116" Width="37.8"/>
        <TableColumn Name="Column117" Width="37.8"/>
        <TableColumn Name="Column118" Width="37.8"/>
        <TableColumn Name="Column119" Width="75.6"/>
        <TableColumn Name="Column120" Width="75.6"/>
        <TableColumn Name="Column121" Width="37.8"/>
        <TableColumn Name="Column122" Width="37.8"/>
        <TableColumn Name="Column123" Width="151.2"/>
        <TableColumn Name="Column124" Width="151.2"/>
        <TableColumn Name="Column125" Width="151.2"/>
        <TableColumn Name="Column126" Width="37.8"/>
        <TableColumn Name="Column127" Width="56.7"/>
        <TableColumn Name="Column128" Width="37.8"/>
        <TableColumn Name="Column129" Width="75.6"/>
        <TableColumn Name="Column130" Width="132.3"/>
        <TableColumn Name="Column131" Width="37.8"/>
        <TableColumn Name="Column132" Width="75.6"/>
        <TableColumn Name="Column133" Width="132.3"/>
        <TableColumn Name="Column134" Width="75.6"/>
        <TableColumn Name="Column135" Width="37.8"/>
        <TableColumn Name="Column136" Width="151.2"/>
        <TableColumn Name="Column137" Width="75.6"/>
        <TableColumn Name="Column138" Width="37.8"/>
        <TableColumn Name="Column139" Width="37.8"/>
        <TableColumn Name="Column140" Width="75.6"/>
        <TableColumn Name="Column141" Width="37.8"/>
        <TableColumn Name="Column142" Width="151.2"/>
        <TableColumn Name="Column143" Width="37.8"/>
        <TableColumn Name="Column144" Width="75.6"/>
        <TableColumn Name="Column145" Width="151.2"/>
        <TableColumn Name="Column146" Width="113.4"/>
        <TableColumn Name="Column147" Width="151.2"/>
        <TableRow Name="Row5" Height="11.34" AutoSize="true">
          <TableCell Name="Cell197" Fill.Color="White" Font="Calibri, 8pt"/>
          <TableCell Name="Cell198" Border.Lines="All" Fill.Color="WhiteSmoke" Text="1" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell199" Border.Lines="All" Fill.Color="WhiteSmoke" Text="2" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell200" Border.Lines="All" Fill.Color="WhiteSmoke" Text="3" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell201" Border.Lines="All" Fill.Color="WhiteSmoke" Text="4" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell202" Border.Lines="All" Fill.Color="WhiteSmoke" Text="5" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell203" Border.Lines="All" Fill.Color="WhiteSmoke" Text="6" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell204" Border.Lines="All" Fill.Color="WhiteSmoke" Text="7" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell205" Border.Lines="All" Fill.Color="WhiteSmoke" Text="8" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell206" Border.Lines="All" Fill.Color="WhiteSmoke" Text="9" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell207" Border.Lines="All" Fill.Color="WhiteSmoke" Text="10" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell208" Border.Lines="All" Fill.Color="WhiteSmoke" Text="11" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell209" Border.Lines="All" Fill.Color="WhiteSmoke" Text="12" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell210" Border.Lines="All" Fill.Color="WhiteSmoke" Text="13" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell211" Border.Lines="All" Fill.Color="WhiteSmoke" Text="14" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell212" Border.Lines="All" Fill.Color="WhiteSmoke" Text="15" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell213" Border.Lines="All" Fill.Color="WhiteSmoke" Text="16" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell214" Border.Lines="All" Fill.Color="WhiteSmoke" Text="17" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell215" Border.Lines="All" Fill.Color="WhiteSmoke" Text="18" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell216" Border.Lines="All" Fill.Color="WhiteSmoke" Text="19" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell217" Border.Lines="All" Fill.Color="WhiteSmoke" Text="20" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell218" Border.Lines="All" Fill.Color="WhiteSmoke" Text="21" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell219" Border.Lines="All" Fill.Color="WhiteSmoke" Text="22" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell220" Border.Lines="All" Fill.Color="WhiteSmoke" Text="23" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell221" Border.Lines="All" Fill.Color="WhiteSmoke" Text="24" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell222" Border.Lines="All" Fill.Color="WhiteSmoke" Text="25" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell223" Border.Lines="All" Fill.Color="WhiteSmoke" Text="26" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell224" Border.Lines="All" Fill.Color="WhiteSmoke" Text="27" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell225" Border.Lines="All" Fill.Color="WhiteSmoke" Text="28" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell226" Border.Lines="All" Fill.Color="WhiteSmoke" Text="29" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell227" Border.Lines="All" Fill.Color="WhiteSmoke" Text="30" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell228" Border.Lines="All" Fill.Color="WhiteSmoke" Text="31" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell229" Border.Lines="All" Fill.Color="WhiteSmoke" Text="32" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell230" Border.Lines="All" Fill.Color="WhiteSmoke" Text="33" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell231" Border.Lines="All" Fill.Color="WhiteSmoke" Text="34" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell232" Border.Lines="All" Fill.Color="WhiteSmoke" Text="35" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell233" Border.Lines="All" Fill.Color="WhiteSmoke" Text="36" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell234" Border.Lines="All" Fill.Color="WhiteSmoke" Text="37" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell235" Border.Lines="All" Fill.Color="WhiteSmoke" Text="38" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell236" Border.Lines="All" Fill.Color="WhiteSmoke" Text="39" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell237" Border.Lines="All" Fill.Color="WhiteSmoke" Text="40" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell238" Border.Lines="All" Fill.Color="WhiteSmoke" Text="41" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell239" Border.Lines="All" Fill.Color="WhiteSmoke" Text="42" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell240" Border.Lines="All" Fill.Color="WhiteSmoke" Text="43" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell241" Border.Lines="All" Fill.Color="WhiteSmoke" Text="44" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell242" Border.Lines="All" Fill.Color="WhiteSmoke" Text="45" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell243" Border.Lines="All" Fill.Color="WhiteSmoke" Text="46" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell244" Border.Lines="All" Fill.Color="WhiteSmoke" Text="47" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
          <TableCell Name="Cell245" Border.Lines="All" Fill.Color="WhiteSmoke" Text="48" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt"/>
        </TableRow>
      </TableObject>
    </PageHeaderBand>
    <DataBand Name="Data1" Top="293.39" Width="3515.4" Height="11.34" CanGrow="true" DataSource="Table">
      <TableObject Name="Table2" Width="3515.4" Height="11.34">
        <TableColumn Name="Column50" Width="37.8"/>
        <TableColumn Name="Column51" Width="37.8"/>
        <TableColumn Name="Column52" Width="75.6"/>
        <TableColumn Name="Column53" Width="37.8"/>
        <TableColumn Name="Column54" Width="37.8"/>
        <TableColumn Name="Column55" Width="37.8"/>
        <TableColumn Name="Column56" Width="37.8"/>
        <TableColumn Name="Column57" Width="37.8"/>
        <TableColumn Name="Column58" Width="75.6"/>
        <TableColumn Name="Column59" Width="113.4"/>
        <TableColumn Name="Column60" Width="113.4"/>
        <TableColumn Name="Column61" Width="75.6"/>
        <TableColumn Name="Column62" Width="37.8"/>
        <TableColumn Name="Column63" Width="37.8"/>
        <TableColumn Name="Column64" Width="37.8"/>
        <TableColumn Name="Column65" Width="37.8"/>
        <TableColumn Name="Column66" Width="56.7"/>
        <TableColumn Name="Column67" Width="37.8"/>
        <TableColumn Name="Column68" Width="37.8"/>
        <TableColumn Name="Column69" Width="37.8"/>
        <TableColumn Name="Column70" Width="75.6"/>
        <TableColumn Name="Column71" Width="75.6"/>
        <TableColumn Name="Column72" Width="37.8"/>
        <TableColumn Name="Column73" Width="37.8"/>
        <TableColumn Name="Column74" Width="151.2"/>
        <TableColumn Name="Column75" Width="151.2"/>
        <TableColumn Name="Column76" Width="151.2"/>
        <TableColumn Name="Column77" Width="37.8"/>
        <TableColumn Name="Column78" Width="56.7"/>
        <TableColumn Name="Column79" Width="37.8"/>
        <TableColumn Name="Column80" Width="75.6"/>
        <TableColumn Name="Column81" Width="132.3"/>
        <TableColumn Name="Column82" Width="37.8"/>
        <TableColumn Name="Column83" Width="75.6"/>
        <TableColumn Name="Column84" Width="132.3"/>
        <TableColumn Name="Column85" Width="75.6"/>
        <TableColumn Name="Column86" Width="37.8"/>
        <TableColumn Name="Column87" Width="151.2"/>
        <TableColumn Name="Column88" Width="75.6"/>
        <TableColumn Name="Column89" Width="37.8"/>
        <TableColumn Name="Column90" Width="37.8"/>
        <TableColumn Name="Column91" Width="75.6"/>
        <TableColumn Name="Column92" Width="37.8"/>
        <TableColumn Name="Column93" Width="151.2"/>
        <TableColumn Name="Column94" Width="37.8"/>
        <TableColumn Name="Column95" Width="75.6"/>
        <TableColumn Name="Column96" Width="151.2"/>
        <TableColumn Name="Column97" Width="113.4"/>
        <TableColumn Name="Column98" Width="151.2"/>
        <TableRow Name="Row4" Height="11.34" AutoSize="true">
          <TableCell Name="Cell148" Fill.Color="White" HideZeros="true" Font="Calibri, 8pt"/>
          <TableCell Name="Cell149" Border.Lines="All" Text="[Table.document_key]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell150" Border.Lines="All" Text="[Table.mdr_position_status]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell151" Border.Lines="All" Text="[Table.participant_title_ru]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell152" Border.Lines="All" Text="[Table.line]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell153" Border.Lines="All" Text="[Table.mdr_tech_unit_code]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell154" Border.Lines="All" Text="[Table.mdr_sequence_num]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell155" Border.Lines="All" Text="[Table.mdr_type]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell156" Border.Lines="All" Text="[Table.title]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell157" Border.Lines="All" Text="[Table.title_title_en1]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell158" Border.Lines="All" Text="[Table.title_title]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell159" Border.Lines="All" Text="[Table.mark]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell160" Border.Lines="All" Text="[Table.discipline_code]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell161" Border.Lines="All" Text="[Table.mark_code]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell162" Border.Lines="All" Text="[Table.document_type_code]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell163" Border.Lines="All" Text="[Table.document_num_serial]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell164" Border.Lines="All" Text="[Table.document_class]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell165" Border.Lines="All" Text="[Table.gate]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell166" Border.Lines="All" Text="[Table.lang]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell167" Border.Lines="All" Text="[Table.lod_position]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell168" Border.Lines="All" Text="[Table.result_type]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell169" Border.Lines="All" Text="[Table.document_statuts_sequence]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell170" Border.Lines="All" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell171" Border.Lines="All" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell172" Border.Lines="All" Text="[Table.doc_number_customer]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell173" Border.Lines="All" Text="[Table.doc_name_en]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell174" Border.Lines="All" Text="[Table.doc_name_rus]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell175" Border.Lines="All" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell176" Border.Lines="All" Text="[Table.release_target]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell177" Border.Lines="All" Text="[Table.revision]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell178" Border.Lines="All" Text="[Table.adress_customer_last_update_date_fact]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Formats>
              <DateFormat Format="dd.MM.yyyy"/>
              <GeneralFormat/>
              <GeneralFormat/>
            </Formats>
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell179" Border.Lines="All" Text="[Table.adress_customer_last_transmittal_num]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell180" Border.Lines="All" Text="[Table.approve_code]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell181" Border.Lines="All" Text="[Table.date]" HideZeros="true" Format="Date" Format.Format="dd.MM.yyyy" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell182" Border.Lines="All" Text="[Table.transmittal_out_number]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell183" Border.Lines="All" Text="[Table.crs_reissue]" HideZeros="true" Format="Date" Format.Format="dd.MM.yyyy" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell184" Border.Lines="All" Text="[Table.approve_code_previous]" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell185" Border.Lines="All" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell186" Border.Lines="All" HideZeros="true" Format="Date" Format.Format="dd.MM.yyyy" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell187" Border.Lines="All" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell188" Border.Lines="All" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell189" Border.Lines="All" HideZeros="true" Format="Date" Format.Format="dd.MM.yyyy" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell190" Border.Lines="All" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell191" Border.Lines="All" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell192" Border.Lines="All" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell193" Border.Lines="All" HideZeros="true" Format="Date" Format.Format="dd.MM.yyyy" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell194" Border.Lines="All" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell195" Border.Lines="All" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
          <TableCell Name="Cell196" Border.Lines="All" HideZeros="true" HorzAlign="Center" VertAlign="Center" Font="Calibri, 8pt">
            <Highlight>
              <Condition Expression="[Table.mdr_position_status] == &quot;CANCELLED&quot;" TextFill.Color="Black" Font="Calibri, 8.25pt, style=Strikeout" ApplyTextFill="false" ApplyFont="true"/>
              <Condition Expression="[Table.mdr_position_status] != &quot;CANCELLED&quot;" Font="Calibri, 8.25pt" ApplyTextFill="false" ApplyFont="true"/>
            </Highlight>
          </TableCell>
        </TableRow>
      </TableObject>
    </DataBand>
    <PageFooterBand Name="PageFooter1" Top="308.73" Width="3515.4" Height="18.9"/>
  </ReportPage>
</Report>
';



-- вставка/обновление шаблона в БД

if (not exists(select 1 
               from dbo.fr_report fr 
               where fr.action_code_report like @action_code_report))
begin 
  begin try 
    begin tran;

    insert dbo.fr_report (  nm_report , fr_param , fr_descr , nm_alias , action_code_report , mask_file , meta_description , fr_source )
                  values ( @nm_report ,@fr_param ,@fr_descr ,@nm_alias ,@action_code_report ,@mask_file ,@meta_description ,@fr_source collate Latin1_General_100_CI_AS_KS_SC_UTF8 );

    commit tran;
  end try 
  begin catch 
    if @@trancount>0 rollback tran;
    throw;
  end catch;
end else begin
  begin try 
    begin tran;
    
    update dbo.fr_report set 
      nm_report         = @nm_report
      ,fr_param         = @fr_param
      ,fr_descr         = @fr_descr
      ,nm_alias         = @nm_alias
      ,mask_file        = @mask_file
      ,meta_description = @meta_description
      ,fr_source        = @fr_source collate Latin1_General_100_CI_AS_KS_SC_UTF8 
    where 
	  action_code_report like @action_code_report;

    commit tran;
  end try 
  begin catch 
    if @@trancount>0 rollback tran;
    throw;
  end catch;
end;



-- подключение шаблона к конструктору

select 
    top(1) @bo_id = bo_id 
  from csi.csi_business_object 
  where action_code like rtrim(@action) and dt_delete is null;
  select @cnt_bo = @@rowcount;

if (@cnt_bo > 0 
    and 
    not exists(select 1 
               from csi.csi_bo2fast_report fr 
               where fr.action_code_report = @action_code_report and fr.bo_id = @bo_id))
begin 
  begin try 
    begin tran;

    insert csi.csi_bo2fast_report (  bo_id , dt_create , dt_update ,dt_delete , create_user_id ,[type] , action_code_report , author_sys , csi_rec_code)
                           values ( @bo_id ,@dt        ,@dt        ,null      ,@create_user_id ,@type   ,@action_code_report ,@author_sys ,@csi_rec_code);

    commit tran;
  end try 
  begin catch 
    if @@trancount>0 rollback tran;
    throw;
  end catch;
end 
else 
if (@cnt_bo > 0 
    and 
    exists(select 1 
               from csi.csi_bo2fast_report fr 
               where fr.action_code_report = @action_code_report and fr.bo_id = @bo_id))
begin
  begin try 
    begin tran;

    update csi.csi_bo2fast_report set
      dt_update     = @dt
      ,[type]       = @type
	  ,author_sys   = @author_sys
	  ,csi_rec_code = @csi_rec_code
    where 
	  action_code_report = @action_code_report and bo_id = @bo_id

    commit tran;
  end try 
  begin catch 
    if @@trancount>0 rollback tran;
    throw;
  end catch;
end;



-- добавление операции "Сформировать отчет" в БО

declare
        @idoper   T_OPER,
        @idlstdoc int;

select 
      @idoper   = idoper
	, @idlstdoc = idlstdoc 
  from tbl_lstdoc  
  where 
    actioncode = ltrim(rtrim(@action));

exec pm_action_add @idoper, @idlstdoc, 'BTN3';
